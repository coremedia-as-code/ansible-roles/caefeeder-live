# CoreMedia - caefeeder-live

## Ports

```
40805
40899
40898
40880
40809
```

## dependent services

### database

```
caefeeder_live_database:
  host: backend_database.int
  port: 3306
  schema: cm_caefeeder
  user: cm_caefeeder
  password: cm_caefeeder
```

### solr

```
caefeeder_live:
  solr:
    url: http://127.0.0.1:40080/solr
```

### content repository

```
caefeeder_live:
  repository:
    url: http://replication-live-server.int:42080/replication-live-server/ior
```
